#!/bin/bash

# Check if a directory path is provided as an argument
if [ $# -ne 2 ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

# Get the directory path from the argument
directory="$1"
output="$2"

# Check if the provided path is a directory
if [ ! -d "$directory" ]; then
    echo "$directory is not a directory."
    exit 1
fi

# List only file names in the specified directory
echo "Files in $directory:"
for file in "$directory"/*; do
    if [ -f "$file" ]; then
        (./font-patcher $file --out=$output --complete)
    fi
done
